<div class="row">
    {% if friends.count() %}
        {% for i, friend in friends %}
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="well">
                    <span class="connectionIconWrapper pull-left">
                        {% if friend.user.getHasProfilePic() %}
                            <img src="{{ friend.user.getMediaPath() ~ '/th_profile.jpg' }}" width="32" />
                        {% else %}
                            <div class="media-object glyphicon glyphicon-user connectionIcon"></div>
                        {% endif %}
                    </span>
                    {{ friend.user.getUserName()|e }}
                </div>
            </div>
        {% endfor %}
    {% else %}
        <div class="col-lg-12 col-md-12 col-sm-12">
            Seems you need to add someone as a friend :)
        </div>
    {% endif %}
</div>
