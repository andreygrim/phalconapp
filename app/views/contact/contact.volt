<div class="container">
    <div class="col-lg-6 col-md-8 col-sm-12 center-block">
        {{ flash.output() }}
        <form class="form-horizontal" method="post">
            <fieldset>
                <legend>Send us a message</legend>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">Your Email</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="{{ inputEmail }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputMessage" class="col-lg-2 control-label">Message</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="5" id="inputMessage" name="inputMessage"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        {{ link_to(['for': 'home'],
                        'Cancel',
                        'class': 'btn btn-default') }}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>