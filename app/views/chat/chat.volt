{% set routeName = this.router.getMatchedRoute().getName() %}
<div class="col-xs-8 col-md-10 text-center" style="margin-bottom: 20px;">
    {{ link_to(['for': 'chat'],
    'General Chat',
    'class': 'btn ' ~ (routeName == 'chat' ? 'btn-primary' : 'btn-info') ) }}
    {{ link_to(['for': 'private', 'room' : randomRoom],
    'Private Chat',
    'class': 'btn ' ~ (routeName == 'private' ? 'btn-primary' : 'btn-info') ) }}
    {{ link_to(['for': 'group'],
    'Group Chat',
    'class': 'btn ' ~ (routeName == 'group' ? 'btn-primary' : 'btn-info') ) }}
    {{ link_to(['for': 'private-group', 'room' : randomRoom],
    'Private Group Chat',
    'class': 'btn ' ~ (routeName == 'private-group' ? 'btn-primary' : 'btn-info') ) }}
    {{ link_to(['for': 'roulette'],
    'Roulette',
    'class': 'btn ' ~ (routeName == 'roulette' ? 'btn-primary' : 'btn-info') ) }}
    {{ link_to(['for': 'file'],
    'File',
    'class': 'btn ' ~ (routeName == 'file' ? 'btn-primary' : 'btn-info') ) }}
</div>
<br>
<br>
<div class="col-xs-8 col-md-10">
    <div id="mgVideoChat"></div>
</div>

{{ javascript_include("js/mgVideoChat/mgVideoChat-1.9.0-min.js") }}
<script>
    $(document).ready(function(){
        $('#mgVideoChat').mgVideoChat({
            wsURL: 'ws://justyo.com:8080?room={{ mgJSRoom }}'{{ mgJSParams }}
        });
    });
</script>

{% if session.has('show-chat-ad') %}
    {% do session.remove('show-chat-ad') %}
    {# ads modal #}
    <div class="modal fade" id="ads-modal">
        <div class="modal-dialog text-center" style="margin-top: 188px;">

            <iframe src="//go.padstm.com/?id=196526&t=iframe" style="width:300px;height:250px;border:0;overflow:hidden;"></iframe>

            <button type="button" class="btn btn-default" data-dismiss="modal">No thanks, start continue to JustYo</button>
            <div id="close-modal" style="color: #fff; cursor: pointer;"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#ads-modal').modal('show');
        });
    </script>
{% endif %}
