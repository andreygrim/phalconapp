<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend>{{ user.getUserName()|e }}'s profile</legend>
                {% if signupError is defined %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        {{ signupError }}
                    </div>
                {% endif %}
                {% if usernameUsed %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        This username already taken, please take another one
                    </div>
                {% endif %}
                {% if emailUsed %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        This email already registered with us, try to login
                    </div>
                {% endif %}
                {% if user.getHasProfilePic() %}
                    <img src="{{ user.getMediaPath() ~ '/th_profile.jpg' }}"
                         style="display: block;margin: 5px auto 15px;border: 1px solid #ccc;"/>
                {% endif %}
                <div class="form-group">
                    <label for="inputPic" class="col-lg-2 control-label">Profile Picture</label>
                    <div class="col-lg-10">
                        <input type="file" class="form-control" id="inputPic" name="inputPic">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-10">
                        <input readonly type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="{{ user.getEmail() }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="Username" value="{{ user.getUserName()|e }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Change password" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
