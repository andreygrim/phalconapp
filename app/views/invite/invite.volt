<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12">
        <form class="form-horizontal" method="post">
            <fieldset>
                <legend>Invite your friends on a chat</legend>
                <div class="form-group">
                    <label for="inputEmails" class="col-lg-2 control-label">Emails</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="5" id="inputEmails" name="inputEmails"></textarea>
                        <span class="help-block">Write emails of your friends you wish to invite. 1 per line.</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
