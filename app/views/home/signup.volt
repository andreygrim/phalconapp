<div class="container">
    <div class="col-lg-6 col-md-8 col-sm-12 center-block">
        <form class="form-horizontal" method="post">
            <fieldset>
                <legend>JustYo Registration</legend>
                {% if signupError is defined %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        {{ signupError }}
                    </div>
                {% endif %}
                {% if usernameUsed %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        This username already taken, please take another one
                    </div>
                {% endif %}
                {% if emailUsed %}
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        This email already registered with us, try to login
                    </div>
                {% endif %}
                <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="{{ inputEmail }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="Username" value="{{ inputUsername }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" value="{{ inputPassword }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2 pull-right">
                        {{ link_to(['for': 'home'],
                        'Cancel',
                        'class': 'btn btn-default') }}
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
