<div class="container">
    <div class="page-header" id="banner">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <h1>JustYo</h1>
                <p class="lead">Free live video chat</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12 center-block text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#login-modal">
                    Login
                </button>
            </div>
            <div class="btn-group">
                <a href="/signup" class="btn btn-primary btn-lg">Register</a>
            </div>
        </div>
        <br>
        <div class="col-lg-8 col-md-12 col-sm-12 center-block text-center">
            <p class="text-success" style="font-size: 17px; font-weight: 600;">Click "Register" to create your free JustYo account, or click "Login" and enter any name to begin your free video chat now!</p>
        </div>
        <div class="text-center">
            <iframe src="//go.padstm.com/?id=196526&t=iframe" style="width:300px;height:250px;border:0;overflow:hidden;"></iframe>
        </div>
    </div>
</div>

{#login modal window#}
<div class="modal fade" id="login-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Login now</h4>
            </div>
            <form class="form-horizontal" method="post">
                <div class="modal-body">
                    {% if loginError is defined %}
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            {{ loginError }}
                        </div>
                    {% endif %}
                    <fieldset>
                        <div class="form-group">
                            <label for="inputLogin" class="col-lg-2 control-label">User</label>
                            <div class="col-lg-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputLogin"
                                       placeholder="Username or email"
                                       name="inputLogin">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                            <div class="col-lg-10">
                                <input type="password"
                                       class="form-control"
                                       id="inputPassword"
                                       placeholder="Password"
                                       name="inputPassword">
                                <div class="has-warning">
                                    <span class="help-block">
                                        Do not enter password if you are not registered
                                    </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success btn-sm">Start chatting</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% if loginError is defined %}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#login-modal').modal('show');
            $('#close-modal').on('click',function(){
                console.log('close');
                $('#login-modal').modal('hide');
            });
        });
    </script>
{% endif %}
