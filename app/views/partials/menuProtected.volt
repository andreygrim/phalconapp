{% set routeName = this.router.getMatchedRoute().getName() %}
{% for chat in ['private', 'group', 'private-group', 'roulette', 'file'] %}
    {% if chat == routeName %}
        {% set routeName = 'chat' %}
    {% endif %}
{% endfor %}
<ul class="list-group">
    <li class="list-group-item intro">
        {% if this.auth.isUserLoggedIn() and this.auth.getUser().getHasProfilePic() %}
            <img src="{{ this.auth.getUser().getMediaPath() ~ '/th_profile.jpg' }}" width="50" />
        {% else %}
            <img src="/images/profilePlaceholder.png" width="50"/>
        {% endif %}
        <div class="name">
            {% if this.auth.isUserLoggedIn() %}
                {{ this.auth.getUser().getUserName() }}
                <br>
                {% set friendsCount = this.auth.getUser().getFriendsCount() %}
                {{ friendsCount }} friend{{ friendsCount > 1 ? 's' : '' }}
            {% else %}
                {{ this.auth.getVisitorName() }}
            {% endif %}
        </div>
    </li>
    <li class="list-group-item">
        <div class="form-group has-feedback-left">
            <input type="text" class="form-control" placeholder="Users search" id="userSearch">
            <input type="hidden" id="userID">
            <i class="glyphicon glyphicon-search form-control-feedback-left"></i>
            <i class="glyphicon glyphicon-plus form-control-feedback-right" style="display: none;"></i>
        </div>
    </li>
    <li class="list-group-item{{ routeName == 'chat' ? ' active' : '' }}">
        {{ link_to(['for': 'chat'],
        '<h4 class="list-group-item-heading">Chat</h4>') }}
    </li>
    <li class="list-group-item{{ routeName == 'profile' ? ' active' : '' }}">
        {{ link_to(['for': 'profile'],
        '<h4 class="list-group-item-heading">Profile</h4>') }}
    </li>
    <li class="list-group-item{{ routeName == 'friends' ? ' active' : '' }}">
        {{ link_to(['for': 'friends'],
        '<h4 class="list-group-item-heading">Friends</h4>') }}
    </li>
    <li class="list-group-item{{ routeName == 'invite' ? ' active' : '' }}">
        {{ link_to(['for': 'invite'],
        '<h4 class="list-group-item-heading">Invite Friends</h4>') }}
    </li>
    <li class="list-group-item">
        {{ link_to(['for': 'logout'],
            '<h4 class="list-group-item-heading">Logout</h4>') }}
    </li>
</ul>

{{ javascript_include("js/typeahead.bundle.js") }}

<script type="text/javascript">
$(document).ready(function () {
    $('#userSearch').typeahead({
        minLength: 3,
    },
    {
        name: 'users',
        displayKey: 'userName',
        source: function (query, cb) {
            $.getJSON('/ajax/getUsers/'+query, function(data) {
                cb(data);
            });
        },
        templates: {
            empty: '<div class="empty-message">Nothing like that...</div>',
            suggestion: function (data) {
                var suggestion = '<p>'+ data.userName + '</p>';
                if (data.friended == 1) {
                    suggestion = '<p><strong>'+ data.userName + '</strong></p>';
                }
                return suggestion;
            }
        },
    });
    $(window).on('typeahead:selected', function(ev, item, name){
        if (item.friended != 1) {
            $('.form-control-feedback-right').show();
            $('#userID').val(item.userID);
        }
    });
    $('.form-control-feedback-right').on('click', function () {
        if ($('#userID').val()) {
            $.getJSON('/ajax/friendUser/'+ parseInt($('#userID').val()), function(data) {
                if (data.userName) {
                    $('#friendUser-modal strong').text(data.userName);
                    $('#friendUser-modal').modal('show');
                    $('#userID, #userSearch').val('');
                }
            });
        }
    });
    $('#userSearch').on('input', function (ev) {
        $('.form-control-feedback-right').hide();
    });
});
</script>

<div class="modal fade" id="friendUser-modal">
    <div class="modal-dialog">
        <div class="modal-content col-md-9 center-block">
            <div class="modal-header text-center">
                <h5>You have added <strong></strong> as a friend!</h5>
            </div>
        </div>
    </div>
</div>

<a href='http://trkur.com/108820/4064' target="_blank">
    <img src='/images/180xPanda.png' width="190"/>
</a>
