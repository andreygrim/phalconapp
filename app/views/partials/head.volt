<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ pageTitle is defined ? pageTitle : 'JustYo.com' }}</title>
        <meta name="description" content="{{ metaDescription is defined ? metaDescription : 'In browser video chat, no download required register for a free account or video chat anonymously!' }}">
        <meta name="keywords" content="{{ metaKeywords is defined ? metaKeywords : 'video chat, free video chat, in browser video chat, video chat roulette, free private video chat online, chat video on the Internet, video chat on the Internet, free video chat online'}}">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        {{ javascript_include("js/jquery-2.1.3.min.js") }}
        {{ javascript_include("js/bootstrap.min.js") }}

        {{ stylesheet_link("css/bootstrap.min.css") }}
        {{ stylesheet_link("css/yeti.css") }}
        {{ stylesheet_link("js/mgVideoChat/mgVideoChat-1.9.0.css") }}
        {{ stylesheet_link("css/styles.css") }}

        <!-- Google Analytics -->
        <script>
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-58835319-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- /Google Analytics -->

        <!-- Share buttons -->
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "9f9e64dc-a464-4f37-b422-8b21b404b090", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        <!-- /Share buttons -->

    </head>
    <body>