<footer class="footer text-center">
  <span class="center-helper"></span>
  <p class="footer-text">
      {{ date('Y') }} &copy; <a rel="copyright" href="/">JustYo.com</a>
      &bull; {{ link_to(['for': 'tos'], 'Terms of Service') }}
      &bull; {{ link_to(['for': 'policy'], 'Privacy Policy') }}
      &bull; {{ link_to(['for': 'contact'], 'Contact Us') }}
  </p>
</footer>
</body>
</html>