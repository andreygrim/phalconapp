{{ partial("partials/head") }}

<div class="container">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                {{ link_to(['for': 'home'],
                    '<img src="/images/JustYOlogo_min.png" height="45">',
                    'class': 'navbar-brand') }}
            </div>
            <ul class="nav navbar-nav navbar-right" style="margin-top: 6px;">
                <li><span class='st_facebook_large' displayText='Facebook'></span>
                <li><span class='st_twitter_large' displayText='Tweet'></span>
                <li><span class='st_linkedin_large' displayText='LinkedIn'></span>
                <li><span class='st_pinterest_large' displayText='Pinterest'></span>
                <li><span class='st_email_large' displayText='Email'></span>
            </ul>

            {#
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <b class="caret"></b></a>
                                    <ul class="dropdown-menu" id="examples">
                                        <li><a href="#page1">Page1</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
            #}
        </div>
    </nav>
</div>

{{ content() }}

{{ partial("partials/footer") }}
