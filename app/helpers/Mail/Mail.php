<?php

namespace Justyo\Mail;

use \Phalcon\Mvc\User\Component;

class Mail extends Component
{
    protected $mail;

    public function __construct()
    {
        $this->mail = new \PHPMailer;
        $this->mail->isSMTP();
        $this->mail->Host = $this->config->mail->host;
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $this->config->mail->user;
        $this->mail->Password = $this->config->mail->pass;
        $this->mail->SMTPSecure = $this->config->mail->secure;
        $this->mail->Port = $this->config->mail->port;
        $this->mail->From = $this->config->mail->host;
        $this->mail->FromName = $this->config->mail->from;
        $this->mail->isHTML(true);

        return $this;
    }

    public function setTo($email, $name = null)
    {
        $this->mail->addAddress($email, $name);

        return $this;
    }

    public function setSubject($subj)
    {
        $this->mail->Subject = $subj;

        return $this;
    }

    public function setBody($body, $altBody = null)
    {
        $this->mail->Body = $body;
        if ($altBody) {
            $this->mail->AltBody = $altBody;
        }

        return $this;
    }

    public function send()
    {
        if (!$this->mail->send()) {
            //log error
        }
    }

    public function sendMail($to, $subject, $template)
    {
        $this->setTo($to)
            ->setSubject($subject)
            ->setBody($this->view->getRender($template))
            ->send();
    }
}