<?php

namespace Justyo\Auth;

use Phalcon\Mvc\User\Component;

class Auth extends Component
{

    public function logInUser($user)
    {
        $this->session->set('user', $user);
        $this->cookies->set($this->config->mgChat->cookieName, $user->getUserName());
    }

    public function logOut()
    {
        $this->session->destroy();
        $this->cookies->delete($this->config->mgChat->cookieName);
    }

    public function isUserLoggedIn()
    {
        return $this->session->has('user');
    }

    public function isVisitorLoggedIn()
    {
        return $this->session->has('visitor');
    }

    public function logInVisitor($login)
    {
        $this->session->set("visitor", $login);
        $this->cookies->set($this->config->mgChat->cookieName, $login);
    }

    public function getVisitorName()
    {
        return $this->isVisitorLoggedIn() ? $this->session->get('visitor') : false;
    }

    public function getUserID()
    {
        return $this->isUserLoggedIn() ? $this->session->get('user')->getUserID() : 0;
    }

    public function getUser()
    {
        return $this->session->get('user');
    }

}