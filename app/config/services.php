<?php
use \Phalcon\DI\FactoryDefault;
use \Phalcon\Http\Request;
use \Phalcon\Mvc\Dispatcher;
use \Phalcon\Mvc\View;
use \Phalcon\Mvc\View\Engine\Volt;
use \Phalcon\Security;
use Justyo\Auth\Auth;

//Create a DI
$di = new FactoryDefault();

$di->setShared('config', $config);

$di->set('dispatcher', function () use ($di) {
    $eventsManager = $di->getShared('eventsManager');
    if (extension_loaded('newrelic')) {
        $eventsManager->attach('dispatch', function ($event, $dispatcher) {
            $controller = $dispatcher->getControllerName();
            $action = $dispatcher->getActionName();
            newrelic_set_appname('justyo.com');
            newrelic_name_transaction($controller . '/' . $action);
        });
    }
    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
},
    true
);

$di->set('db', function () use ($config) {
    try {
        $db = new \Phalcon\Db\Adapter\Pdo\Mysql([
            "host"     => $config->database->host,
            "username" => $config->database->username,
            "password" => $config->database->password,
            "dbname"   => $config->database->dbname
        ]);
    } catch (Exception $e) {
        die("<b>Error when initializing database connection:</b> " . $e->getMessage());
    }

    return $db;
});


$di->set('security', function () {
    $security = new Security();
    $security->setWorkFactor(12);

    return $security;
}, true);


//Registering Volt as template engine
$di->set(
    'view',
    function () use ($config) {
        $view = new View();
        $view->setViewsDir($config->phalcon->viewsDir);
        $view->registerEngines(
            [
                ".volt" => function ($view, $di) use ($config) {
                    $volt = new Volt($view, $di);
                    $volt->setOptions(
                        [
                            'compiledPath'  => $config->phalcon->voltCacheDir,
                            'stat'          => true,
                            'compileAlways' => true
                        ]
                    );

                    return $volt;
                }
            ]
        );

        return $view;
    }
);

$di->set(
    'router',
    function () use ($config) {
        require $config->phalcon->routes;

        return $router;
    }
);

$di->set('cookies', function () {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);

    return $cookies;
});


$di->set('session', function () use ($di) {
    $session = new Justyo\Session\Adapter\Database([
        'db'    => $di->getDb(),
        'table' => 'session_data'
    ]);
    session_name('justyoSession');
    $session->start();

    return $session;
});

$di->set('flash', function () {
    $flash = new Phalcon\Flash\Session([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning',
    ]);

    return $flash;
});

$di->setShared('auth', function () {
    $auth = new Auth();

    return $auth;
});

return $di;
