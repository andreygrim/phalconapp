<?php

$router = new \Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);

$router->notFound([
    'controller' => 'error',
    'action'     => 'notFound',
]);

// Main routes
$router->add("/", [
    'controller' => 'home',
    'action'     => 'index',
])->setName('home');

$router->add("/terms-of-service", [
    'controller' => 'home',
    'action'     => 'tos',
])->setName('tos');

$router->add("/privacy-policy", [
    'controller' => 'home',
    'action'     => 'policy',
])->setName('policy');

$router->add("/signup", [
    'controller' => 'home',
    'action'     => 'signup',
])->setName('home');

$router->add("/logout", [
    'controller' => 'home',
    'action'     => 'logout',
])->setName('logout');

$router->add("/contact", [
    'controller' => 'contact',
    'action'     => 'index',
])->setName('contact');

$router->add("/chat", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('chat');

$router->add("/chat/private/{room:[0-9]{4,5}}", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('private');

$router->add("/chat/group", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('group');

$router->add("/chat/private-group/{room:[0-9]{4,5}}", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('private-group');

$router->add("/chat/roulette", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('roulette');

$router->add("/chat/file", [
    'controller' => 'chat',
    'action'     => 'index',
])->setName('file');

$router->add("/friends", [
    'controller' => 'friends',
    'action'     => 'index',
])->setName('friends');

$router->add("/profile", [
    'controller' => 'profile',
    'action'     => 'index',
])->setName('profile');

$router->add("/invite", [
    'controller' => 'invite',
    'action'     => 'index',
])->setName('invite');


// AJAX routes
$ajaxRoutes = new \Phalcon\Mvc\Router\Group([
    'controller' => 'ajax'
]);
$ajaxRoutes->setPrefix('/ajax');

$ajaxRoutes->add('/getUsers/{name:.{3,}}', [
    'action' => 'getUsers'
]);

$ajaxRoutes->add('/friendUser/{friendUserID:[0-9]+}', [
    'action' => 'friendUser'
]);

$router->mount($ajaxRoutes);

return $router;