<?php

$config = new Phalcon\Config([
    'database' => [
        'host'     => 'localhost',
        'username' => 'username',
        'password' => 'password',
        'dbname'   => 'dbname'
    ],
    'phalcon'  => [
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'voltCacheDir'   => __DIR__ . '/../../app/tmp/voltCache/',
        'helpersDir'     => __DIR__ . '/../../app/helpers/',
        'PHPMailer'      => __DIR__ . '/../../app/helpers/Mail/class.phpmailer.php',
        'SMTP'           => __DIR__ . '/../../app/helpers/Mail/class.smtp.php',
        'routes'         => __DIR__ . '/routes.php',
        'services'       => __DIR__ . '/services.php',
    ],
    'mgChat'   => [
        'cookieName' => 'mgVideoChatSimple',
        'fileTransferLimit' => 1024000,
    ],
    'mail'     => [
        'host'   => 'host',
        'port'   => 587,
        'secure' => 'tls',
        'user'   => 'user',
        'pass'   => 'pass',
        'from'   => 'from',
    ],
]);
