<?php
namespace Justyo\Models;

class Users extends \Phalcon\Mvc\Model
{
    protected $userID;
    protected $userName;
    protected $email;
    protected $dateCreated;
    protected $password;
    protected $hasProfilePic;

    public function setUserID($userID)
    {
        $this->userID = intval($userID);

        return $this;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getHasProfilePic()
    {
        return $this->hasProfilePic > 0;
    }

    public function setHasProfilePic($has)
    {
        $this->hasProfilePic = $has ? 1 : 0;

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        if (!isset($this->dateCreated)) {
            $this->setDateCreated(date('Y-m-d H:i:s'));
        }
        if (!isset($this->hasProfilePic)) {
            $this->setHasProfilePic(0);
        }
    }

    public function getSource()
    {
        return 'users';
    }

    public function initialize()
    {
        $this->hasMany("userID", "Friends", "userID", [
            'alias' => 'friends'
        ]);
        $this->belongsTo("userID", "Friends", "friendUserID", [
            'alias' => 'friendOf'
        ]);
    }

    protected $mediaPath;

    public function getMediaPath()
    {
        if (!$this->mediaPath) {
            $this->afterFetch();
        }

        return $this->mediaPath;
    }

    public function afterFetch()
    {
        $i = sprintf('%06d', intval($this->getUserID()));
        $this->mediaPath = "/users-media/{$i[0]}{$i[1]}/{$i[2]}{$i[3]}/$i";
    }

    public function getFriendsCount()
    {
        return Friends::count(['userID = :userID:', 'bind' => ['userID' => $this->getUserID()]]);
    }

}
