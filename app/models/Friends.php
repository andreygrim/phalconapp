<?php
namespace Justyo\Models;

use \Phalcon\DI;

class Friends extends \Phalcon\Mvc\Model
{
    protected $userID;
    protected $friendUserID;
    protected $dateCreated;

    public function setUserID($userID)
    {
        $this->userID = intval($userID);

        return $this;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setFriendUserID($friendUserID)
    {
        $this->friendUserID = $friendUserID;

        return $this;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function beforeValidationOnCreate()
    {
        if (!isset($this->dateCreated)) {
            $this->setDateCreated(date('Y-m-d H:i:s'));
        }
    }

    public function getSource()
    {
        return 'friends';
    }

    public function initialize()
    {
        $this->belongsTo("userID", '\Justyo\Models\Users', "userID");
        $this->hasOne("friendUserID", '\Justyo\Models\Users', "userID", [
            'alias' => 'user'
        ]);
    }

    public static function getFriendsForUserID($userID)
    {
        $di = DI::getDefault();
        $result = $di->getModelsManager()->createBuilder()
            ->columns('friend.*, user.*')
            ->addFrom('Justyo\Models\Friends', 'friend')
            ->leftJoin('Justyo\Models\Users', 'user.userID = friend.friendUserID', 'user')
            ->where('friend.userID = :userID:', ['userID' => $userID])
            ->getQuery()
            ->execute();

        return $result;
    }

}
