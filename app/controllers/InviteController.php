<?php

use Justyo\Mail\Mail;

class InviteController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        if (!$this->auth->isUserLoggedIn() && !$this->auth->isVisitorLoggedIn()) {
            $this->auth->logOut();
            $this->response->redirect(['for' => 'home']);

            return $this->response;
        }

        if ($emails = $this->request->get('inputEmails')) {
            $emailsArray = explode("\n", $emails);
            $count = 0;
            foreach ($emailsArray as $one) {
                if ($email = filter_var(trim($one), FILTER_VALIDATE_EMAIL)) {
                    $sendMail = new Mail();
                    $sendMail->setTo($email)
                        ->setSubject('You are invited to JustYo')
                        ->setBody("Hi,<br><br>You are receiving this email, because your friend would like to video chat with you on <a href=\"http://justyo.com/\">JustYo</a>.<br><br>Yo!")
                        ->send();
                    $count++;
                }
            }
            if ($count) {
                $this->flash->success("{$count} of your friends are invited!");
            } else {
                $this->flash->error("No valid emails were found :(");
            }
        }

        $this->view->setTemplateAfter('loggedIn');

        $this->view->pick('invite/invite');
    }

}
