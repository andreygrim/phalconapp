<?php

use Justyo\Mail\Mail;

class ContactController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        if ($this->request->isPost()) {
            if (!$email = filter_var($this->request->getPost('inputEmail', 'email'), FILTER_VALIDATE_EMAIL)) {
                $this->flash->error('We really need your email ;)');
            }
            if (!$message = nl2br(htmlspecialchars($this->request->getPost('inputMessage', 'striptags')))) {
                $this->flash->error('Nothing to read in your message...');
            }
            if ($email && $message) {
                $sendMail = new Mail();
                $sendMail->setTo('solcomllc@gmail.com')
                    ->setSubject('New message from JustYo contact form')
                    ->setBody("<b>From:</b> {$email}<br><br><b>Message:</b> {$message}")
                    ->send();
                $this->flash->success("We've received your message! Thank you.");
            }
        }

        $this->view->pick('contact/contact');
        $this->view->setVar('metaKeywords', 'video chat, chat online');
    }

}
