<?php

use Justyo\Models\Friends;

class FriendsController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        if (!$this->auth->isUserLoggedIn()) {
            if (!$this->auth->isVisitorLoggedIn()) {
                $this->auth->logOut();
                $this->response->redirect(['for' => 'home']);

                return $this->response;
            } else {
                $this->flash->warning('You have to be registered :)');
                $this->response->redirect(['for' => 'chat']);

                return $this->response;
            }
        }
        $friends = Friends::getFriendsForUserID($this->auth->getUserID());
        $this->view->setTemplateAfter('loggedIn');
        $this->view->pick('friends/friends');
        $this->view->setVar('friends', $friends);
    }

}
