<?php

use Justyo\Models\Users;
use Justyo\Models\Friends;
use \Phalcon\DI;

class AjaxController extends \Phalcon\Mvc\Controller
{

    public function getUsersAction($name)
    {
        if (!$this->auth->isUserLoggedIn() || !$this->request->isAjax()) {
            $this->response->setStatusCode(401, 'Unauthorized');

            return $this->response;
        }
        $name = '%' . $name . '%';
        $userID = $this->auth->getUserID();
        $di = DI::getDefault();
        $result = $di->getModelsManager()->createBuilder()
            ->columns('user.userID, user.userName, IF(friend.userID,1,0) as friended')
            ->addFrom('Justyo\Models\Users', 'user')
            ->leftJoin('Justyo\Models\Friends', 'user.userID = friend.friendUserID AND friend.userID = :userID:',
                'friend')
            ->where('user.userID != :userID: AND user.userName LIKE :name:')
            ->limit(10)
            ->getQuery()
            ->execute(['userID' => $userID, 'name' => $name])
            ->toArray();

        $this->response->setJsonContent($result);

        return $this->response;
    }

    public function friendUserAction($friendUserID)
    {
        if (!$this->auth->isUserLoggedIn() || !$this->request->isAjax()) {
            $this->response->setStatusCode(401);

            return $this->response;
        }
        $userID = $this->auth->getUserID();
        $friend = Users::findFirst(intval($friendUserID));

        if ($friend) {
            $newFriend = new Friends();
            $newFriend->setUserID($userID)
                ->setFriendUserID($friendUserID)
                ->save();
            $this->response->setJsonContent(['userName' => $friend->getUserName()]);
        }

        return $this->response;
    }

}
