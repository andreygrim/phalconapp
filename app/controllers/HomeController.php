<?php

use Justyo\Models\Users;

class HomeController extends \Phalcon\Mvc\Controller
{

    public function beforeExecuteRoute($dispatcher)
    {
        // This is executed before every found action
        // return false; // to prevent execution.... i think
    }

    public function indexAction()
    {
        if ($login = $this->request->get('inputLogin', 'striptags')) {
            if ($password = $this->request->get('inputPassword')) {
                // login registered User
                $user = Users::findFirst([
                    'UPPER(userName) = UPPER(:login:) OR UPPER(email) = UPPER(:login:)',
                    'bind' => ['login' => $login]
                ]);
                if ($user && $this->security->checkHash($password, $user->getPassword())) {
                    $this->auth->logInUser($user);
                    $this->flash->success("Successfully logged in");
                    $this->session->set('show-chat-ad', true);
                } else {
                    $this->view->setVar('loginError', 'Such Username + password combination does not exist :(');
                }
            } else {
                // login Visitor without password
                $user_exist = Users::count([
                    'UPPER(userName) = UPPER(:userName:)',
                    'bind' => ['userName' => $login]
                ]);
                if ($user_exist) {
                    $this->view->setVar('loginError', 'This Username already registered. Please, choose other one.');
                } else {
                    $this->auth->logInVisitor($login);
                    $this->flash->success("Successfully logged in");
                    $this->session->set('show-chat-ad', true);
                }
            }
        }

        if ($this->auth->isUserLoggedIn() || $this->auth->isVisitorLoggedIn()) {
            // redirect to chat
            $this->response->redirect(['for' => 'chat']);

            return $this->request;
        }

        $this->view->pick('home/home');
        $this->view->setVar('pageTitle', 'Just yo! Free video chat online');
    }

    public function logoutAction()
    {
        $this->auth->logOut();
        $this->response->redirect(['for' => 'home']);
    }

    public function signupAction()
    {
        $email = null;
        $username = null;
        $password = null;
        $emailUsed = null;
        $usernameUsed = null;
        if ($this->auth->isUserLoggedIn()) {
            $this->response->redirect(['for' => 'chat']);

            return $this->response;
        }
        if ($this->request->isPost()) {
            $email = filter_var($this->request->getPost('inputEmail', 'email'), FILTER_VALIDATE_EMAIL);
            $username = $this->request->getPost('inputUsername', 'striptags');
            $password = $this->request->getPost('inputPassword');
            if ($email && $username && $password) {
                $emailUsed = Users::count(['UPPER(email) = UPPER(:email:)', 'bind' => ['email' => $email]]);
                $usernameUsed = Users::count([
                    'UPPER(userName) = UPPER(:username:)',
                    'bind' => ['username' => $username]
                ]);
                if (!$emailUsed && !$usernameUsed) {
                    $user = new Users();
                    $user->setEmail($email)
                        ->setPassword($this->security->hash($password))
                        ->setUserName($username);
                    $user->save();
                    $this->flash->success("You've been successfully registered and logged in! Welcome!");
                    $this->session->set('show-chat-ad', true);
                    $this->auth->logInUser($user);
                    $this->response->redirect(['for' => 'chat']);
                }
            } else {
                $this->view->setVar('signupError', 'Each field is required');
            }
        } else {
            if ($this->auth->isVisitorLoggedIn()) {
                $username = $this->auth->getVisitorName();
            }
        }

        $this->view->pick('home/signup');
        $this->view->setVar('inputEmail', $email);
        $this->view->setVar('inputUsername', $username);
        $this->view->setVar('inputPassword', $password);
        $this->view->setVar('emailUsed', $emailUsed);
        $this->view->setVar('usernameUsed', $usernameUsed);
    }

    public function tosAction()
    {
        $this->view->pick('static/tos');
        $this->view->setVar('metaKeywords', '');
    }

    public function policyAction()
    {
        $this->view->pick('static/policy');
        $this->view->setVar('metaKeywords', '');
    }

}
