<?php

use Justyo\Models\Users;

class ProfileController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        if (!$this->auth->isUserLoggedIn()) {
            if (!$this->auth->isVisitorLoggedIn()) {
                $this->auth->logOut();
                $this->response->redirect(['for' => 'home']);

                return $this->response;
            } else {
                $this->flash->warning('You have to be registered :)');
                $this->response->redirect(['for' => 'chat']);

                return $this->response;
            }
        }

        $user = Users::findFirst(intval($this->auth->getUserID()));
        if ($username = $this->request->get('inputUsername', 'striptags')) {
            $usernameUsed = Users::count(['UPPER(userName) = UPPER(:username:)', 'bind' => ['username' => $username]]);
            if (!$usernameUsed) {
                $user->setUserName($username);
            }
        }

        if ($password = $this->request->getPost('inputPassword')) {
            $user->setPassword($this->security->hash($password));
        }

        if ($this->request->hasFiles()) {
            $file = $this->request->getUploadedFiles()[0];
            $image = new \Imagick();
            $image->pingImage($file->getTempName());
            if ($image->readImage($file->getTempName())) {
                $image->cropThumbnailImage(100, 100);
                $path = dirname($_SERVER["SCRIPT_FILENAME"]) .
                    $user->getMediaPath() .
                    '/th_profile.jpg';
                mkdir(dirname($path), 0777, true);
                $image->writeImage($path);
                $user->setHasProfilePic(true);
            } else {
                $this->flash->warning('Wrong image type, try JPG or PNG');
            }
            $image->destroy();
        }

        if ($this->request->isPost()) {
            if ($user->save()) {
                $this->flash->success("Successfully saved");
                $this->auth->logInUser($user);
            } else {
                $this->flash->error("Save failed");
            }
        }

        $this->view->setTemplateAfter('loggedIn');

        $this->view->pick('profile/profile');
        $this->view->setVar('user', $user);
    }

}
