<?php

class ChatController extends \Phalcon\Mvc\Controller
{

    public function indexAction($room)
    {
        if (!$this->auth->isVisitorLoggedIn() && !$this->auth->isUserLoggedIn()) {
            $this->auth->logOut();
            $this->response->redirect(['for' => 'home']);
        }

        $this->setMgVideoJs($room);

        $this->view->setTemplateAfter('loggedIn');

        $this->view->pick('chat/chat');

        $this->view->setVar('randomRoom', mt_rand(1000, 99999));
    }

    protected function setMgVideoJs($room)
    {
        $routeName = $this->router->getMatchedRoute()->getName();
        switch ($routeName) {
            case 'private':
                $number = $room;
                $this->flash->notice('Share this <code id="privateUrl">http://justyo.com/chat/private/' .
                    $room . '</code> private url to someone to join you here.');
                break;
            case 'group':
                $number = 5;
                break;
            case 'private-group':
                $number = 'group_' . $room;
                $this->flash->notice('Share this <code id="privateUrl">http://justyo.com/chat/private-group/' .
                    $room . '</code> private url to someone to join you here.');
                break;
            case 'roulette':
                $number = 6;
                break;
            case 'file':
                $number = 7;
                $params = ",fileMaxSize: {$this->config->mgChat->fileTransferLimit}";
                break;
            default:
                $number = 1;
        }
        $this->view->setVar('mgJSRoom', $number);
        $this->view->setVar('mgJSParams', $params);
    }

}
