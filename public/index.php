<?php

$debug = new \Phalcon\Debug();
$debug->listen();

//config
require __DIR__ . '/../app/config/config.php';

//Register an autoloader
$loader = new \Phalcon\Loader();
$loader->registerDirs([
        $config->phalcon->controllersDir,
]);
$loader->registerNamespaces([
    'Justyo'                    => $config->phalcon->helpersDir,
    'Justyo\Models'             => $config->phalcon->modelsDir,
]);
$loader->registerClasses([
    'PHPMailer'         => $config->phalcon->PHPMailer,
    'SMTP'              => $config->phalcon->SMTP,
]);
$loader->register();

//load DI
require $config->phalcon->services;

//Handle the request
$application = new \Phalcon\Mvc\Application($di);

echo $application->handle()->getContent();
